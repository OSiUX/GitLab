#!/bin/bash

mkdir -p public

for i in *.md
do
  o="public/$(basename "$i" .md).html"
  pandoc -f markdown "$i" -t html -o "$o"
done
