# Git Flow

```bash

master
+
|       develop
+------>+
|       |
|       +
|       |       feature a
|       +------>+
|       |       |
|       |       +
|       |       |
|       +<------+
|       |
|       |               release
|       +-------------->+
|       |               |
+<------+<--------------+ v0.1.0
|       |
|       |       feature b
|       +------>+
|       |       |
|       |       +
|       |       |
|       +<------+
|       |
|       |               release
|       +-------------->+
|       |               |
+<------+<--------------+ v0.2.0
|       |
|       |                       hotfix
+-------------------------------+
|       |                       |
+<------+<----------------------+ v0.1.1
|       |
|       |

```

## Inicializar git-flow

```bash

git flow init

  Which branch should be used for bringing forth production releases?
     - develop
     - master
  Branch name for production releases: [master]

  Which branch should be used for integration of the "next release"?
     - develop
  Branch name for "next release" development: [develop]

  How to name your supporting branch prefixes?
  Feature branches? [feature/]
  Bugfix branches? [bugfix/]
  Release branches? [release/]
  Hotfix branches? [hotfix/]
  Support branches? [support/]
  Version tag prefix? [v]
  Hooks and filters directory? [/home/osiris/.git/hooks]

```

## Comenzar un _feature_

```bash

git flow feature start basic

  Cambiado a nueva rama 'feature/basic'

  Summary of actions:
  - A new branch 'feature/basic' was created, based on 'develop'
  - You are now on branch 'feature/basic'

  Now, start committing on your feature. When done, use:

       git flow feature finish basic

```

## Finalizar un _feature_

```bash

git flow feature finish basic

  Cambiado a rama 'develop'
  Ya está actualizado.
  Eliminada la rama feature/basic (era dc11bd0)..

  Summary of actions:
  - The feature branch 'feature/basic' was merged into 'develop'
  - Feature branch 'feature/basic' has been locally deleted
  - You are now on branch 'develop'

```

## Comenzar un _release_

```bash

git flow release start 0.2.0

  Cambiado a nueva rama 'release/0.2.0'

  Summary of actions:
  - A new branch 'release/0.2.0' was created, based on 'develop'
  - You are now on branch 'release/0.2.0'

  Follow-up actions:
  - Bump the version number now!
  - Start committing last-minute fixes in preparing your release
  - When done, run:

       git flow release finish '0.2.0'

```

## Finalizar un _release_

```bash

git flow release finish 0.2.0

  Cambiado a rama 'master'
  Eliminada la rama release/0.2.0 (era dc11bd0)..

  Summary of actions:
  - Release branch 'release/0.2.0' has been merged into 'master'
  - The release was tagged 'v0.2.0'
  - Release branch 'release/0.2.0' has been locally deleted
  - You are now on branch 'master'

```

## Comentar el _tag_

```bash

first release
#
# Escribe un mensaje para la tag:
#   v0.2.0
# Las líneas que comienzan con '#' serán ignoradas.

```

## Verificar _tags_

```bash

git tag

  v0.2.0

git tag -n

  v0.2.0    first release

```
