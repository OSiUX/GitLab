# Flujo de trabajo

```bash

+-----> git pull -----> git commit -----> git push ---->+
^                                                       |
|                                                       v
+<------------------------------------------------------+

```

## Listar _branchs_ locales

```bash

git branch

```

## Listar _branchs_ remotos

```bash

git branch -r

```

## Crear un _branch_

```bash

git branch develop

```

## Cambiar de _branch_

```bash

git checkout develop

```

## Fusionar _branchs_

```bash

git checkout master
git merge develop

```

```bash

git cherry pick hash

```
